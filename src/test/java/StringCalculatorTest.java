import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class StringCalculatorTest {

    @Test
    public void shouldReturnZeroWhenStringIsEmpty(){
        assertEquals(0, StringCalculator.add(""));
    }

    @Test
    public void shouldReturnNumberIfOnlyOneNumberProvided(){
        String number = "1";
        assertEquals(Integer.parseInt(number), StringCalculator.add(number));
    }

    @Test
    public void shouldReturnSumOfTwoNumbers(){
        assertEquals(6, StringCalculator.add("1,5"));
    }

    @Test
    public void shouldReturnSumOfAnyAmountOfNumbers(){
        assertEquals(14, StringCalculator.add("1,2,5,6"));
    }

    @Test
    public void shouldReturnSumOfNumbersWithNewLineBetweenNumbers(){
        assertEquals(6, StringCalculator.add("1\n2,3"));
    }

    @Test
    public void shouldReturnSumOfNumbersWithCustomDelimiter(){
        assertEquals(3, StringCalculator.add("//;\n1;2"));
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWithAllNegativesInMessage(){
        try{
            StringCalculator.add("-1,-3,4");
            fail();
        }catch(IllegalArgumentException e){
            assertEquals("Negatives not allowed: -1, -3", e.getMessage());
        }
    }

    @Test
    public void shouldIgnoreNumbersBiggerThan1000(){
        assertEquals(1004, StringCalculator.add("1,999,1001,4"));
    }

    @Test
    public void shouldSumNumbersWithCustomMultiCharacterDelimiter(){
        assertEquals(6, StringCalculator.add("//[***]\n1***2***3"));
    }

    @Test
    public void shouldSumNumbersWithMultipleCustomDelimiters(){
        assertEquals(6, StringCalculator.add("//[*][%]\n1*2%3"));
    }

    @Test
    public void shouldSumNumbersWithMultipleCustomMultiCharactersDelimiters(){
        assertEquals(6, StringCalculator.add("//[**aa][%d%]\n1**aa2%d%3"));
    }

}
