import java.util.Arrays;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StringCalculator {

    private String delimiter;
    private String numbers;

    private static final String NEGATIVES_NOT_ALLOWED_EXCEPTION = "Negatives not allowed: ";
    private static final String DEFAULT_DELIMITER = ",";
    private static final int MAX_NUMBER_VALUE = 1000;


    private StringCalculator(String delimiter, String numbers) {
        this.delimiter = delimiter;
        this.numbers = numbers;
    }

    public static int add(String numbers){
        if(isEmpty(numbers)){return 0;}

        StringCalculator calculator = getInitializedStringCalculator(numbers);
        String[] splitNumbers = splitInputIntoNumbers(calculator);
        return getNumbersSum(splitNumbers);
    }

    private static StringCalculator getInitializedStringCalculator(String numbers) {
        if(hasCustomDelimiter(numbers)){
            return getStringCalculatorWithCustomerDelimiter(numbers);
        }else{
            return new StringCalculator(DEFAULT_DELIMITER, numbers);
        }
    }

    private static StringCalculator getStringCalculatorWithCustomerDelimiter(String numbers) {
        String[] elements = numbers.split("\n", 2);
        String delimiter = getCustomDelimiter(elements[0]);
        return new StringCalculator(delimiter, elements[1]);
    }

    private static String getCustomDelimiter(String element) {
        String delimiter = element
                .substring(2)
                .replaceAll("^\\[+", "")
                .replaceAll("]+$", "");
        return Arrays.stream(delimiter.split("]\\["))
                .map(Pattern::quote)
                .collect(Collectors.joining("|"));
    }

    private static boolean hasCustomDelimiter(String numbers) {
        return numbers.startsWith("//");
    }

    private static int getNumbersSum(String[] splitNumbers) {
        Supplier<IntStream> supplier = () -> Arrays.stream(splitNumbers).mapToInt(Integer::valueOf);
        checkForNegatives(supplier);
        return supplier.get()
                .filter(e -> e < MAX_NUMBER_VALUE)
                .sum();
    }

    private static void checkForNegatives(Supplier<IntStream> supplier) {
        String negatives = supplier.get()
                .filter(e -> e < 0)
                .mapToObj(Integer::toString)
                .collect(Collectors.joining(", "));

        if(!isEmpty(negatives)){
            throw new IllegalArgumentException(NEGATIVES_NOT_ALLOWED_EXCEPTION + negatives);
        }
    }

    private static String[] splitInputIntoNumbers(StringCalculator calculator) {
        return calculator.numbers.split(calculator.delimiter + "|\n");
    }

    private static boolean isEmpty(String string){
        return string == null || string.isEmpty();
    }

}
